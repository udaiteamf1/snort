###
# Copyright 2020 TeamF1 Networks Pvt. Ltd.
#
# modification history 
# -------------------- 
# 01a,29Jan2019,uk  created
###

snort: FORCE
	@if [ ! -e $(SNORT_PREFIX)/etc/snort/ ]; then \
		$(MAKE) -C comps/gpl/snort build; \
	fi

export SNORT:=snort