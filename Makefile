###
# Copyright 2020 TeamF1 Networks Pvt. Ltd.
#
# modification history 
# -------------------- 
# 01a,29Jan2019,uk  created
###
export ROOT:=$(shell pwd)
export SNORT_PATH=comps/gpl/snort
export SNORT_PREFIX=$(ROOT)/teamf1-snort

all: snort_bld

snort_bld : FORCE
	mkdir -p $(SNORT_PREFIX)
	$(MAKE) -C $(SNORT_PATH) build
	chmod -R g-s $(SNORT_PREFIX)
	dpkg-deb --build $(SNORT_PREFIX)
	mkdir -p Deliverables/
	cp teamf1-snort*.deb Deliverables

FORCE: ;

include $(SNORT_PATH)/snort.mk
